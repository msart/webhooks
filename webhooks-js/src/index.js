
const http = require("http");
const url = require('url');
const createHmac = require('crypto');
const host = 'localhost';
const port = 3000;

const validation = function (request, res){
  const payload = request.body;

  const timestamp = request.headers['x-mindsight-timestamp'];

  const signature = request.headers['x-mindsight-sha256-signature'];

  const message = `${timestamp}.${JSON.stringify(payload)}`;

  const hmac = createHmac('sha256', this.webhookSecret);
  hmac.update(message);
  const computedSignature = hmac.digest('hex');

  return computedSignature === signature;
}
var params=function(req){
  let q=req.url.split('?'),result={};
  if(q.length>=2){
      q[1].split('&').forEach((item)=>{
           try {
             result[item.split('=')[0]]=item.split('=')[1];
           } catch (e) {
             result[item.split('=')[0]]='';
           }
      })
  }
  return result;
}

const requestListener = function (request, res) {
  const url = request.url.split("?")[0];
  console.log(url);
  if (url ==='/json/'){
    res.writeHead(200);
    const reqparams = params(request);
    res.end(JSON.stringify({ "verification": reqparams.verification }));
  }
  else if (url ==='/'){
    res.writeHead(200);
    const reqparams = params(request);
  
    res.end(JSON.stringify({ "verification": "ois" }));
  }
  else{
    res.writeHead(200);
    const reqparams = params(request);
    res.end(JSON.stringify({ "verification": "oi" }));
  }
  
};

const server = http.createServer(requestListener);
server.listen(port, host, () => {
    console.log(`Server is running on http://${host}:${port}`);
});